// Design tokens
const { aliases: spacingAliases } = require('@de-electron/design-tokens/dist/web/spacing.raw.json');
const { aliases: colorAliases } = require('@de-electron/design-tokens/dist/web/colors.raw.json');
const {
  aliases: borderRadiusAliases,
} = require('@de-electron/design-tokens/dist/web/border-radius.raw.json');
const {
  aliases: breakpointAliases,
} = require('@de-electron/design-tokens/dist/web/breakpoints.raw.json');
const {
  aliases: particleAliases,
} = require('@de-electron/design-tokens/dist/web/particles.raw.json');
const { aliases: fontAliases } = require('@de-electron/design-tokens/dist/web/font-scale.raw.json');
const {
  aliases: containerAliases,
} = require('@de-electron/design-tokens/dist/web/containers.raw.json');
const { aliases: iconAliases } = require('@de-electron/design-tokens/dist/web/icons.raw.json');

// Functions to make properties
const makeProperties = (inputObj) => (removeString = null) => {
  const makeObject = (acc, key) => ({
    ...acc,
    [key.split(removeString).pop()]: inputObj[key].value,
  });
  return Object.keys(inputObj).reduce(makeObject, '');
};

function fontScale(aliases) {
  const propertyPrefixes = [
    'font-letter-spacing-',
    'font-line-height-',
    'font-size-',
    'font-family-',
  ];
  function check(item) {
    return propertyPrefixes.find((condition) => item.includes(condition));
  }
  function makeMap(acc, item) {
    const prefix = check(item);
    const whatToAdd = { [item]: aliases[item] };
    return { ...acc, [prefix]: { ...acc[prefix], ...whatToAdd } };
  }
  const sortedMap = Object.keys(aliases).reduce(makeMap, {});
  const [space, line, size, family] = Object.values(sortedMap);

  return { space, line, size, family };
}

const widthHeightUtil = (config, e) => (
  properties,
  property = properties.slice(0, properties.length - 1)
) =>
  Object.entries(config(`theme[${properties}]`)).map(([key, value]) => ({
    [`.${e([property, key].join('-'))}`]: { height: value, width: value },
  }));

// Define properties
const spacing = makeProperties(spacingAliases)('spacing-');
const colors = makeProperties(colorAliases)('color-');
const borderRadius = makeProperties(borderRadiusAliases)('border-radius-');
const screens = makeProperties(breakpointAliases)('breakpoint-');
const particles = makeProperties(particleAliases)('particle-');
const { space, line, size, family } = fontScale(fontAliases);
const fontSize = makeProperties(size)('font-size-');
const letterSpacing = makeProperties(space)('font-letter-spacing-');
const lineHeight = makeProperties(line)('font-line-height-');
const fontFamily = makeProperties(family)('font-family-');
const maxWidth = makeProperties(containerAliases)('container-');
const icons = makeProperties(iconAliases)('icon-');

module.exports = {
  purge: ['src/**/*.js', 'src/**/*.jsx', 'src/**/*.ts', 'src/**/*.tsx', 'public/**/*.html'],
  theme: {
    colors,
    borderRadius,
    fontFamily: {
      ...fontFamily,
      primary: '"NewsGothicBT-Roman", Helvetica, Arial, sans-serif',
      light: '"NewsGothicBT-Light", Helvetica, Arial, sans-serif',
    },
    fontSize,
    icons,
    letterSpacing,
    lineHeight: { ...lineHeight, none: '1' },
    maxWidth,
    particles,
    screens: {
      ...screens,
      // alias single letter keys to two letter keys
      sm: screens.s,
      md: screens.m,
      lg: screens.l,
    },
    spacing,
  },
  corePlugins: {
    float: false,
  },
  variants: {
    cursor: ['responsive', 'hover', 'focus'],
  },
  plugins: [
    function ({ addUtilities, addComponents, config, theme, e }) {
      // Custom 'button' component
      const buttons = {
        '.btn': {
          padding: '.5rem 1rem',
          borderRadius: '.25rem',
          fontWeight: '600',
        },
        '.btn-blue': {
          backgroundColor: theme('colors.blue'),
          color: theme('colors.white'),
          '&:hover': {
            backgroundColor: theme('colors.blue-dark'),
          },
        },
        '.btn-red': {
          backgroundColor: '#e3342f',
          color: theme('colors.white'),
          '&:hover': {
            backgroundColor: '#cc1f1a',
          },
        },
      };
      const widthHeight = widthHeightUtil(config, e);
      const particles = widthHeight('particles');
      const icons = widthHeight('icons');

      addUtilities([particles, icons], ['responsive']);
      addComponents(buttons);
    },
  ],
};
