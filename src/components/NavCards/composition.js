const getNavCardsItems = (fields, modalList = []) => {
  if (!fields) {
    return [];
  }

  const getLink = (curr) => {
    // If the navcard links to a modal video, get that link. If, not return the regular link
    const modalLink = modalList.find((elm) => elm.fields.Parent.Id === curr.ModalList?.Id);

    return modalLink
      ? { hasModal: true, link: modalLink.fields.Video.value.href }
      : { hasModal: false, link: curr.PodItemLink?.value?.href };
  };

  return fields.Children.filter((elm) => elm.ShouldDisplayableComponent).reduce(
    (acc, curr) => [
      ...acc,
      {
        cta: {
          href: curr.PodItemLink.value.href,
          text: curr.PodCtaText?.value?.text,
        },
        id: curr.Id,
        image: { ...curr.PodImage.value },
        text: curr.PodText.value,
        title: curr.Headline.value || curr.PodItemLink.value?.text,
        ...getLink(curr),
      },
    ],
    []
  );
};

export default getNavCardsItems;
