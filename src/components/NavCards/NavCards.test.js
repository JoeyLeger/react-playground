import React from 'react';
import { mount } from 'enzyme';
import { expect } from 'chai';
import NavCardsComponent from './NavCardsComponent';
import getNavCardsItems from './composition';
import { Data } from './NavCards.data';

const getItems = getNavCardsItems(Data.rectangular.fields);

describe('NavCards', () => {
  const wrapper = mount(<NavCardsComponent items={getItems} />);

  it('renders correctly', () => {
    mount(<NavCardsComponent items={getItems} />);
  });

  it('renders the correct amount of items', () => {
    expect(wrapper.find('.de-cards__card')).to.have.lengthOf(getItems.length);
  });

  it('renders the correct title', () => {
    expect(wrapper.find('.de-cards__title').first().text().trim()).to.equal(getItems[0].title);
  });

  it('renders the correct image', () => {
    expect(wrapper.find('.de-cards__media').first().find('[src]').props().src).to.equal(
      getItems[0].image.src
    );
  });

  it('renders the correct link href', () => {
    expect(wrapper.find('.de-cards__title__link').first().find('[href]').props().href).to.equal(
      getItems[0].link
    );
  });
});
