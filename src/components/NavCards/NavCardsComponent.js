import React, { Fragment } from 'react';
import './NavCards.scss';

const NavCardsComponent = ({ isOrdered = false, items, variant = 'rectangular' }) => {
  const ListElement = ({ children, className }) => {
    return isOrdered ? (
      <ol className={className}>{children}</ol>
    ) : (
      <ul className={className}>{children}</ul>
    );
  };

  const RenderItemTitle = ({ hasModal, link, title }) => {
    const itemClass =
      'de-cards__title__link text-left text-teal-dark-x border-b-0 no-underline hover:cursor-pointer';
    const ItemTitle = () => <span>{title}</span>;
    const ItemWrapper = ({ children }) => <h3 className='de-cards__title mb-m'>{children}</h3>;
    const LinkWrapper = ({ children }) =>
      hasModal ? (
        <button className={itemClass}>{children}</button>
      ) : (
        <a href={link} className={itemClass}>
          {children}
        </a>
      );

    return link ? (
      <ItemWrapper>
        <LinkWrapper>
          <ItemTitle />
        </LinkWrapper>
      </ItemWrapper>
    ) : (
      <ItemWrapper>
        <ItemTitle />
      </ItemWrapper>
    );
  };

  const RenderContent = ({ cta, link, text }) => {
    if (variant === 'bigicon') return null;

    return (
      <Fragment>
        <div
          className='de-cards__description flex-grow'
          dangerouslySetInnerHTML={{ __html: text }}
        />
        {cta.text ? (
          <a
            href={link}
            className='xs:block hidden xs:mt-s md:mt-m text-teal-dark-x border-b-0 no-underline hover:cursor-pointer'
          >
            <button>{cta.text}</button>
          </a>
        ) : null}
      </Fragment>
    );
  };

  return (
    <div className={`de-cards de-cards--${variant} max-w-m l:max-w-l xl:max-w-3xl my-zero mx-auto`}>
      <ListElement className='de-cards__group flex justify-center p-zero list-none'>
        {items.map(({ cta, hasModal, id, image, link, text, title }) => (
          <li key={id} className='de-cards__item relative'>
            <div className='de-cards__card flex flex-row md:flex-col relative h-full text-left'>
              <img
                alt='alt'
                src={image.src}
                srcSet={`${image.src} 256w, ${image.src} 512w`}
                sizes='(min-width: 996px) 276px, 65px'
                className='de-cards__media img-fluid block flex-grow-0 flex-shrink-0 mr-m overflow-hidden mb-s md:mb-m'
              />
              <div className='de-cards__body flex-grow sm:flex sm:flex-col'>
                <RenderItemTitle hasModal={hasModal} link={link} title={title} />
                <RenderContent cta={cta} link={link} text={text} />
              </div>
            </div>
          </li>
        ))}
      </ListElement>
    </div>
  );
};

export default NavCardsComponent;
