export const Data = {
  rectangular: {
    uid: 'f30b6388-7a3a-4b44-9384-250b7891e531',
    componentName: 'Navigation Pod',
    dataSource: '{CDCFC924-2547-4364-97FE-FA1E952ACF9A}',
    params: {
      Caching: '0|0|0|0|0|0|0|0',
      ShowJumpLinkImage: '1',
    },
    fields: {
      Icon: {
        value: {
          src:
            'https://scdev25.duke-energy.com/_/media/images/svg/lights/iconoutdoorlight.svg?h=100&la=en&w=100',
          alt: 'Image',
          width: '100',
          height: '100',
        },
      },
      Headline: {
        value: 'Products &amp; Services',
      },
      Subhead: {
        value: 'Find the products and services you need to bring energy efficiency to light.',
      },
      CTARichText: {
        value: '',
      },
      CallToAction: {
        value: {
          href: '/home/products',
          id: '{08E0F9B5-D1C6-46F5-AB60-A79A582636A4}',
          querystring: '',
          text: 'View All',
          title: 'View All',
          class: '',
          url: '/Public/Home/Home/Products',
          linktype: 'internal',
        },
      },
      ModalList: null,
      Children: [
        {
          PodItemLink: {
            value: {
              href: '/home/products/home-energy-house-call',
              text: '',
              linktype: 'internal',
              class: '',
              title: '',
              querystring: '',
              id: '{868CABBB-099D-490C-9176-9BB24D5CD55C}',
            },
          },
          MobilePodItemLink: {
            value: {
              href: '',
            },
          },
          PodImage: {
            value: {
              src:
                'https://scdev25.duke-energy.com/_/media/images/rectangular-pod/for-your-home-landing/rectangular-pod-hehc.png?h=190&la=en&w=300',
              alt: 'rectangular pod hehc',
              width: '300',
              height: '190',
            },
          },
          PodText: {
            value: 'Closely scrutinize your home.',
          },
          PodCtaText: {
            value: '',
          },
          Headline: {
            value: 'House Looker',
          },
          ModalList: null,
          ShouldDisplayableComponent: true,
          Id: 'c6921e62-7563-4d77-ac8d-d870babf3eeb',
          TemplateId: '9a32b57e-563e-45dd-b8aa-aaee54de3249',
          Version: 2,
          Url: '/home/content/prod-and-serv-ky/home-energy-house-call',
          Path:
            '/sitecore/content/Public/Home/Home/Content/Prod and Serv KY/Home Energy House Call',
          Name: 'Home Energy House Call',
          RelativeUrl: '/home',
        },
        {
          PodItemLink: {
            value: {
              href: '/home/products/power-manager',
              text: 'Big impact...little effort',
              linktype: 'internal',
              class: '',
              title: 'Big impact...little effort',
              querystring: '',
              id: '{A8D54E70-A807-4F6B-B9D1-071CBCFA8F8A}',
            },
          },
          MobilePodItemLink: {
            value: {
              href: '',
            },
          },
          PodImage: {
            value: {
              src:
                'https://scdev25.duke-energy.com/_/media/images/rectangular-pod/for-your-home-landing/rectangular-pod-power-manager-ewh.jpg?h=190&la=en&w=300',
              alt: 'rectangular pod power manager ewh',
              width: '300',
              height: '190',
            },
          },
          PodText: {
            value: "A lot of people don't know that money actually has a shelf life.",
          },
          PodCtaText: {
            value: '',
          },
          Headline: {
            value: 'Here, take a dollar sign.',
          },
          ModalList: null,
          ShouldDisplayableComponent: true,
          Id: '1f8cf85c-b9b2-49e6-90e7-e1c1a5511250',
          TemplateId: '9a32b57e-563e-45dd-b8aa-aaee54de3249',
          Version: 4,
          Url: '/home/content/prod-and-serv-ky/powermanager',
          Path: '/sitecore/content/Public/Home/Home/Content/Prod and Serv KY/Powermanager',
          Name: 'Powermanager',
          RelativeUrl: '/home',
        },
        {
          PodItemLink: {
            value: {
              href: '/home/products/smart-saver/referral-program',
              text: 'Contractor Referrals',
              linktype: 'internal',
              class: '',
              title: '',
              querystring: '',
              id: '{909CC8F2-B980-49D2-98EE-AA3A84742D28}',
            },
          },
          MobilePodItemLink: {
            value: {
              href: '',
            },
          },
          PodImage: {
            value: {
              src:
                'https://scdev25.duke-energy.com/_/media/images/rectangular-pod/for-your-home-landing/rectangular-pod-contractor-referral.jpg?h=190&la=en&w=300',
              alt: 'rectangular pod contractor referral',
              width: '300',
              height: '190',
            },
          },
          PodText: {
            value:
              'I mean, when I think of a hat, I think of a ballcap or possibly a fedora. Hey, remember when those were cool?',
          },
          PodCtaText: {
            value: '',
          },
          Headline: {
            value: "Why do they call it a hard hat? Shouldn't it be a helmet or something?",
          },
          ModalList: null,
          ShouldDisplayableComponent: true,
          Id: '4fh060fc-e9e8-4e8f-bbe1-27f1f6b188b4',
          TemplateId: '9a32b57e-563e-45dd-b8aa-aaee54de3249',
          Version: 6,
          Url: '/home/content/prod-and-serv-ky/smart-saver-referral',
          Path: '/sitecore/content/Public/Home/Home/Content/Prod and Serv KY/Smart Saver Referral',
          Name: 'Smart Saver Referral',
          RelativeUrl: '/home',
        },
        {
          PodItemLink: {
            value: {
              href: '/home/products/my-home-energy-interactive',
              text: 'My Home Energy Interactive',
              linktype: 'internal',
              class: '',
              title: '',
              querystring: '',
              id: '{A19776CB-9D72-4EA1-8821-3A26CD0A0762}',
            },
          },
          MobilePodItemLink: {
            value: {
              href: '',
            },
          },
          PodImage: {
            value: {
              src:
                'https://scdev25.duke-energy.com/_/media/images/rectangular-pod/for-your-home-landing/rectangular-pod-my-her-interactive.jpg?h=190&la=en&w=300',
              alt: 'rectangular pod my her interactive',
              width: '300',
              height: '190',
            },
          },
          PodText: {
            value:
              'Things are not looking good. I mean, it has been a rough quarter for everyone, but I just thought we could turn things around, you know?',
          },
          PodCtaText: {
            value: '',
          },
          Headline: {
            value: 'Down and Right',
          },
          ModalList: null,
          ShouldDisplayableComponent: true,
          Id: 'f74f781f-8477-4234-9375-eaae592e844f',
          TemplateId: '9a32b57e-563e-45dd-b8aa-aaee54de3249',
          Version: 5,
          Url: '/home/content/prod-and-serv-ky/myher-interactive',
          Path: '/sitecore/content/Public/Home/Home/Content/Prod and Serv KY/MyHER Interactive',
          Name: 'MyHER Interactive',
          RelativeUrl: '/home',
        },
        {
          PodItemLink: {
            value: {
              href: '/home/products/my-home-energy-interactive',
              text: 'My Home Energy Interactive2',
              linktype: 'internal',
              class: '',
              title: '',
              querystring: '',
              id: '{A19776CB-9D72-4EA1-8821-3A26CD0A0762}',
            },
          },
          MobilePodItemLink: {
            value: {
              href: '',
            },
          },
          PodImage: {
            value: {
              src:
                'https://scdev25.duke-energy.com/_/media/images/rectangular-pod/for-your-home-landing/rectangular-pod-my-her-interactive.jpg?h=190&la=en&w=300',
              alt: 'rectangular pod my her interactive',
              width: '300',
              height: '190',
            },
          },
          PodText: {
            value: 'They can pry it from my cold, dead hands.',
          },
          PodCtaText: {
            value: '',
          },
          Headline: {
            value: "We Probably Shouldn't Have Bought the Lexus",
          },
          ModalList: null,
          ShouldDisplayableComponent: true,
          Id: 'f74f7g9f-8477-4234-9375-eaae592e844f',
          TemplateId: '9a32b57e-563e-45dd-b8aa-aaee54de3249',
          Version: 5,
          Url: '/home/content/prod-and-serv-ky/myher-interactive',
          Path: '/sitecore/content/Public/Home/Home/Content/Prod and Serv KY/MyHER Interactive',
          Name: 'MyHER Interactive',
          RelativeUrl: '/home',
        },
        {
          PodItemLink: {
            value: {
              href: '/home/products/my-home-energy-interactive',
              text: 'My Home Energy Interactive3',
              linktype: 'internal',
              class: '',
              title: '',
              querystring: '',
              id: '{A19776CB-9D72-4EA1-8821-3A26CD0A0762}',
            },
          },
          MobilePodItemLink: {
            value: {
              href: '',
            },
          },
          PodImage: {
            value: {
              src:
                'https://scdev25.duke-energy.com/_/media/images/rectangular-pod/for-your-home-landing/rectangular-pod-my-her-interactive.jpg?h=190&la=en&w=300',
              alt: 'rectangular pod my her interactive',
              width: '300',
              height: '190',
            },
          },
          PodText: {
            value: 'Things have been stressful at home lately.',
          },
          PodCtaText: {
            value: '',
          },
          Headline: {
            value: "Oh, it's a bar chart. I get it now. Sorry for the overreaction.",
          },
          ModalList: null,
          ShouldDisplayableComponent: true,
          Id: 'g74f789f-8477-4234-9375-eaae592e844f',
          TemplateId: '9a32b57e-563e-45dd-b8aa-aaee54de3249',
          Version: 5,
          Url: '/home/content/prod-and-serv-ky/myher-interactive',
          Path: '/sitecore/content/Public/Home/Home/Content/Prod and Serv KY/MyHER Interactive',
          Name: 'MyHER Interactive',
          RelativeUrl: '/home',
        },
        {
          PodItemLink: {
            value: {
              href: '/home/products/my-home-energy-interactive',
              text: 'My Home Energy Interactive4',
              linktype: 'internal',
              class: '',
              title: '',
              querystring: '',
              id: '{A19776CB-9D72-4EA1-8821-3A26CD0A07622}',
            },
          },
          MobilePodItemLink: {
            value: {
              href: '',
            },
          },
          PodImage: {
            value: {
              src:
                'https://scdev25.duke-energy.com/_/media/images/rectangular-pod/for-your-home-landing/rectangular-pod-my-her-interactive.jpg?h=190&la=en&w=300',
              alt: 'rectangular pod my her interactive',
              width: '300',
              height: '190',
            },
          },
          PodText: {
            value: "Access your home's data online for new energy-saving options.",
          },
          PodCtaText: {
            value: '',
          },
          Headline: {
            value: 'My Home Energy Interactive4',
          },
          ModalList: null,
          ShouldDisplayableComponent: true,
          Id: 'f74f789f-8477-4234-9375-eaae092e844f2',
          TemplateId: '9a32b57e-563e-45dd-b8aa-aaee54de3249',
          Version: 5,
          Url: '/home/content/prod-and-serv-ky/myher-interactive',
          Path: '/sitecore/content/Public/Home/Home/Content/Prod and Serv KY/MyHER Interactive',
          Name: 'MyHER Interactive',
          RelativeUrl: '/home',
        },
        {
          PodItemLink: {
            value: {
              href: '/home/products/my-home-energy-interactive',
              text: 'My Home Energy Interactive5',
              linktype: 'internal',
              class: '',
              title: '',
              querystring: '',
              id: '{A19776CB-9D72-4EA1-8821-3A26CD0A0762}',
            },
          },
          MobilePodItemLink: {
            value: {
              href: '',
            },
          },
          PodImage: {
            value: {
              src:
                'https://scdev25.duke-energy.com/_/media/images/rectangular-pod/for-your-home-landing/rectangular-pod-my-her-interactive.jpg?h=190&la=en&w=300',
              alt: 'rectangular pod my her interactive',
              width: '300',
              height: '190',
            },
          },
          PodText: {
            value: "Access your home's data online for new energy-saving options.",
          },
          PodCtaText: {
            value: '',
          },
          Headline: {
            value: 'My Home Energy Interactive5',
          },
          ModalList: null,
          ShouldDisplayableComponent: true,
          Id: 'f74gg89f-8477-4234-9375-eaae592e844f3',
          TemplateId: '9a32b57e-563e-45dd-b8aa-aaee54de3249',
          Version: 5,
          Url: '/home/content/prod-and-serv-ky/myher-interactive',
          Path: '/sitecore/content/Public/Home/Home/Content/Prod and Serv KY/MyHER Interactive',
          Name: 'MyHER Interactive',
          RelativeUrl: '/home',
        },
      ],
      ShouldDisplayableComponent: true,
      Id: 'cdcfc924-2547-4364-97fe-fa1e952acf9a',
      TemplateId: '66568e6d-ddc4-4b90-8237-33f21c3fd7b1',
      Version: 10,
      Url: '/home/content/prod-and-serv-ky',
      Path: '/sitecore/content/Public/Home/Home/Content/Prod and Serv KY',
      Name: 'Prod and Serv KY',
      RelativeUrl: '/home',
      AnalyticsHeadline: 'Products &amp; Services',
      RenderSelector: 'Rectangular',
      BackgroundColor: 'white-bg',
      OrderingType: 1,
      PodType: 0,
      IsEmptyHeadline: false,
    },
  },
  centered: {
    uid: '81180ecd-7119-4611-812e-57095a5dc22f',
    componentName: 'Navigation Pod',
    dataSource: '{3949159E-DC11-409C-BAE9-4DC4D60F916E}',
    params: {
      ShowJumpLinkImage: '1',
      'Ordering Type': '{8A84C908-1CCA-4EEC-88F3-3C6C346834D8}',
      'Render Selector': '{8E9B6D55-7D1F-4B21-9054-97BF50D879AC}',
      'Background Color': '{E02CEEC1-46EF-4C79-8AAC-43831CFF2334}',
    },
    fields: {
      Icon: {
        value: {},
      },
      Headline: {
        value: '',
      },
      Subhead: {
        value: '',
      },
      CTARichText: {
        value:
          '<a href="/home/billing/billing-payment-options">COMPARE PAYMENT METHODS</a><br />\n<br />',
      },
      CallToAction: {
        value: {
          href: '',
        },
      },
      ModalList: null,
      Children: [
        {
          PodItemLink: {
            value: {
              href: '/my-account/sign-in',
              text: '',
              linktype: 'internal',
              class: '',
              title: '',
              querystring: '',
              id: '{416298DF-33D4-4347-9D27-08F66D922A96}',
            },
          },
          MobilePodItemLink: {
            value: {
              href: '',
            },
          },
          PodImage: {
            value: {
              src:
                'https://scdev25.duke-energy.com/_/media/images/svg/money/icon-desktop-mobile-dollar.svg?h=100&la=en&w=100',
              alt: 'icon desktop mobile dollar',
              width: '100',
              height: '100',
            },
          },
          PodText: {
            value: "Here's some short text: <a href='/home/billing/paperless'>Learn More</a>",
          },
          PodCtaText: {
            value: 'Pay Now',
          },
          Headline: {
            value: 'Pay online',
          },
          ModalList: null,
          ShouldDisplayableComponent: true,
          Id: 'cf07a189-dfce-4cb9-a4a5-4ca48838c79b',
          TemplateId: '9a32b57e-563e-45dd-b8aa-aaee54de3249',
          Version: 11,
          Url: '/home/billing/content/payment-options-ky/pay-online-paperless-billing',
          Path:
            '/sitecore/content/Public/Home/Home/Billing/Content/Payment Options KY/Pay Online Paperless Billing',
          Name: 'Pay Online Paperless Billing',
          RelativeUrl: '/home/billing',
        },
        {
          PodItemLink: {
            value: {
              href: '/home/billing/billing-payment-options/pay-now',
              text: 'Pay Now',
              linktype: 'internal',
              class: '',
              title: '',
              querystring: '',
              id: '{8B4B3103-6CEC-43F8-9BCD-F022BDD6813C}',
            },
          },
          MobilePodItemLink: {
            value: {
              href: '',
            },
          },
          PodImage: {
            value: {
              src:
                'https://scdev25.duke-energy.com/_/media/images/svg/money/icon-desktop-mobile-dollar.svg?h=100&la=en&w=100',
              alt: 'icon credit card',
              width: '100',
              height: '100',
            },
          },
          PodText: {
            value:
              'Slightly larger text that should wrap two lines or so.&nbsp;<a href="/home/billing/credit-card-debit-card-or-electronic-check-payments">Learn More</a><br />',
          },
          PodCtaText: {
            value: 'Pay Now',
          },
          Headline: {
            value: 'Pay using a credit card, debit card or eCheck',
          },
          ModalList: null,
          ShouldDisplayableComponent: true,
          Id: '3089bd5b-2d01-4fd1-bf77-c204fa9095a9',
          TemplateId: '9a32b57e-563e-45dd-b8aa-aaee54de3249',
          Version: 9,
          Url: '/home/billing/content/payment-options-ky/pay-with-credit-card',
          Path:
            '/sitecore/content/Public/Home/Home/Billing/Content/Payment Options KY/Pay with Credit Card',
          Name: 'Pay with Credit Card',
          RelativeUrl: '/home/billing',
        },
        {
          PodItemLink: {
            value: {
              href: '/home/products/duke-energy-app',
              text: 'Download Now',
              linktype: 'internal',
              class: '',
              title: '',
              querystring: '',
              id: '{E4B0A74B-4880-4DF5-B75D-E461410FE8BE}',
            },
          },
          MobilePodItemLink: {
            value: {
              href: '',
            },
          },
          PodImage: {
            value: {
              src:
                'https://scdev25.duke-energy.com/_/media/images/svg/technology/icon-de-app.svg?h=100&la=en&w=100',
              alt: 'icon de app',
              width: '100',
              height: '100',
            },
          },
          PodText: {
            value:
              'View and pay your bill from anywhere, anytime. Also see your billing history and personalized offers.',
          },
          PodCtaText: {
            value: 'Download Now',
          },
          Headline: {
            value: 'Pay using our app',
          },
          ModalList: null,
          ShouldDisplayableComponent: true,
          Id: '607a002f-1a10-4925-bbe6-c36b5dfb7f1d',
          TemplateId: '9a32b57e-563e-45dd-b8aa-aaee54de3249',
          Version: 3,
          Url: '/home/billing/content/payment-options-ky/de-app',
          Path: '/sitecore/content/Public/Home/Home/Billing/Content/Payment Options KY/DE App',
          Name: 'DE App',
          RelativeUrl: '/home/billing',
        },
        {
          PodItemLink: {
            value: {
              href: '/home/billing/payment-locations',
              text: 'Pay in person',
              linktype: 'internal',
              class: '',
              title: '',
              querystring: '',
              id: '{FAE3A981-6EEF-4A78-819B-F8794690A359}',
            },
          },
          MobilePodItemLink: {
            value: {
              href: '',
            },
          },
          PodImage: {
            value: {
              src:
                'https://scdev25.duke-energy.com/_/media/images/svg/money/icon-location-dollar.svg?h=100&la=en&w=100',
              alt: 'icon location dollar',
              width: '100',
              height: '100',
            },
          },
          PodText: {
            value: 'Make a payment in person at one of our preferred locations.<br />',
          },
          PodCtaText: {
            value: 'Find Location',
          },
          Headline: {
            value: 'Pay in person',
          },
          ModalList: null,
          ShouldDisplayableComponent: true,
          Id: 'e9c620aa-7a05-4f6c-ac96-ab8cd050d184',
          TemplateId: '9a32b57e-563e-45dd-b8aa-aaee54de3249',
          Version: 3,
          Url: '/home/billing/content/payment-options-ky/pay-in-person',
          Path:
            '/sitecore/content/Public/Home/Home/Billing/Content/Payment Options KY/Pay in Person',
          Name: 'Pay in Person',
          RelativeUrl: '/home/billing',
        },
        {
          PodItemLink: {
            value: {
              href: '/home/billing/automatic-draft',
              text: 'Automatic Draft',
              linktype: 'internal',
              class: '',
              title: 'Automatic Draft',
              querystring: '',
              id: '{CD602A40-75C7-435D-874E-20430A571DA0}',
            },
          },
          MobilePodItemLink: {
            value: {
              href: '',
            },
          },
          PodImage: {
            value: {
              src:
                'https://scdev25.duke-energy.com/_/media/images/svg/documents/icon-calendar-repeat.svg?h=100&la=en&w=100',
              alt: 'icon calendar repeat',
              width: '100',
              height: '100',
            },
          },
          PodText: {
            value: 'Make an automatic payment by drafting funds from your bank account.',
          },
          PodCtaText: {
            value: 'Continue',
          },
          Headline: {
            value: 'Pay with Automatic Draft',
          },
          ModalList: null,
          ShouldDisplayableComponent: true,
          Id: '62119d2c-6e36-496e-9833-6a9b7fddde97',
          TemplateId: '9a32b57e-563e-45dd-b8aa-aaee54de3249',
          Version: 5,
          Url: '/home/billing/content/payment-options-ky/pay-automatic-draft-in-oh-ky',
          Path:
            '/sitecore/content/Public/Home/Home/Billing/Content/Payment Options KY/Pay Automatic Draft IN OH KY',
          Name: 'Pay Automatic Draft IN OH KY',
          RelativeUrl: '/home/billing',
        },
        {
          PodItemLink: {
            value: {
              href: '/home/billing/automatic-draft',
              text: 'Automatic Draft',
              linktype: 'internal',
              class: '',
              title: 'Automatic Draft',
              querystring: '',
              id: '{CD602A40-75C7-435D-874E-20430A571DA0}',
            },
          },
          MobilePodItemLink: {
            value: {
              href: '',
            },
          },
          PodImage: {
            value: {
              src:
                'https://scdev25.duke-energy.com/_/media/images/svg/documents/icon-calendar-repeat.svg?h=100&la=en&w=100',
              alt: 'icon calendar repeat',
              width: '100',
              height: '100',
            },
          },
          PodText: {
            value:
              "This should also take up a generous amount of space. Then you will know what it looks like when there's a big headline as well as a lot of text.",
          },
          PodCtaText: {
            value: 'Continue',
          },
          Headline: {
            value: 'Pay with Automatic Draft And Take Up More Than Two Lines',
          },
          ModalList: null,
          ShouldDisplayableComponent: true,
          Id: '62119d2c-6e36-496e-9833-6a9b7fddde97',
          TemplateId: '9a32b57e-563e-45dd-b8aa-aaee54de3249',
          Version: 5,
          Url: '/home/billing/content/payment-options-ky/pay-automatic-draft-in-oh-ky',
          Path:
            '/sitecore/content/Public/Home/Home/Billing/Content/Payment Options KY/Pay Automatic Draft IN OH KY',
          Name: 'Pay Automatic Draft IN OH KY',
          RelativeUrl: '/home/billing',
        },
        {
          PodItemLink: {
            value: {
              href: '/home/billing/automatic-draft',
              text: 'Automatic Draft',
              linktype: 'internal',
              class: '',
              title: 'Automatic Draft',
              querystring: '',
              id: '{CD602A40-75C7-435D-874E-20430A571DA0}',
            },
          },
          MobilePodItemLink: {
            value: {
              href: '',
            },
          },
          PodImage: {
            value: {
              src:
                'https://scdev25.duke-energy.com/_/media/images/svg/documents/icon-calendar-repeat.svg?h=100&la=en&w=100',
              alt: 'icon calendar repeat',
              width: '100',
              height: '100',
            },
          },
          PodText: {
            value: 'Make an automatic payment by drafting funds from your bank account.',
          },
          PodCtaText: {
            value: 'Continue',
          },
          Headline: {
            value: 'Pay with Automatic Draft3',
          },
          ModalList: null,
          ShouldDisplayableComponent: true,
          Id: '62119d2c-6e36-496e-9833-6a9b7fddde97',
          TemplateId: '9a32b57e-563e-45dd-b8aa-aaee54de3249',
          Version: 5,
          Url: '/home/billing/content/payment-options-ky/pay-automatic-draft-in-oh-ky',
          Path:
            '/sitecore/content/Public/Home/Home/Billing/Content/Payment Options KY/Pay Automatic Draft IN OH KY',
          Name: 'Pay Automatic Draft IN OH KY',
          RelativeUrl: '/home/billing',
        },
        {
          PodItemLink: {
            value: {
              href: '/home/billing/automatic-draft',
              text: 'Automatic Draft',
              linktype: 'internal',
              class: '',
              title: 'Automatic Draft',
              querystring: '',
              id: '{CD602A40-75C7-435D-874E-20430A571DA0}',
            },
          },
          MobilePodItemLink: {
            value: {
              href: '',
            },
          },
          PodImage: {
            value: {
              src:
                'https://scdev25.duke-energy.com/_/media/images/svg/documents/icon-calendar-repeat.svg?h=100&la=en&w=100',
              alt: 'icon calendar repeat',
              width: '100',
              height: '100',
            },
          },
          PodText: {
            value: 'Make an automatic payment by drafting funds from your bank account.',
          },
          PodCtaText: {
            value: 'Continue',
          },
          Headline: {
            value: 'Pay with Automatic Draft4',
          },
          ModalList: null,
          ShouldDisplayableComponent: true,
          Id: '62119d2c-6e36-496e-9833-6a9b7fddde97',
          TemplateId: '9a32b57e-563e-45dd-b8aa-aaee54de3249',
          Version: 5,
          Url: '/home/billing/content/payment-options-ky/pay-automatic-draft-in-oh-ky',
          Path:
            '/sitecore/content/Public/Home/Home/Billing/Content/Payment Options KY/Pay Automatic Draft IN OH KY',
          Name: 'Pay Automatic Draft IN OH KY',
          RelativeUrl: '/home/billing',
        },
      ],
      ShouldDisplayableComponent: true,
      Id: '3949159e-dc11-409c-bae9-4dc4d60f916e4',
      TemplateId: '66568e6d-ddc4-4b90-8237-33f21c3fd7b1',
      Version: 4,
      Url: '/home/billing/content/payment-options-ky',
      Path: '/sitecore/content/Public/Home/Home/Billing/Content/Payment Options KY',
      Name: 'Payment Options KY',
      RelativeUrl: '/home/billing',
      AnalyticsHeadline: '/sitecore/api/layout/render/jss',
      RenderSelector: 'Centered',
      BackgroundColor: 'white-bg',
      OrderingType: 1,
      PodType: 1,
      IsEmptyHeadline: true,
    },
  },
  bigIcon: {
    uid: 'caafccc7-b5e5-4d4d-bb44-c84c512b1425',
    componentName: 'Navigation Pod',
    dataSource: '{EC51D447-F4FC-4033-A5B6-410D095113AB}',
    params: {
      ShowJumpLinkImage: '1',
      RenderSelector: '{61F714DF-D053-4DF4-9E54-9020A5844A39}',
      BackgroundColor: '{E02CEEC1-46EF-4C79-8AAC-43831CFF2334}',
    },
    fields: {
      Icon: {
        value: {},
      },
      Headline: {
        value: 'More about us',
      },
      Subhead: {
        value: '&nbsp;',
      },
      CTARichText: {
        value: '',
      },
      CallToAction: {
        value: {
          href: '',
        },
      },
      ModalList: null,
      Children: [
        {
          PodItemLink: {
            value: {
              href: '/our-company/about-us/businesses',
              id: '{2C985197-CD58-402B-8A22-2F6C461DA356}',
              querystring: '',
              text: '',
              title: '',
              class: '',
              url: '/Public/Home/Our Company/About Us/Businesses',
              linktype: 'internal',
            },
          },
          MobilePodItemLink: {
            value: {
              href: '',
            },
          },
          PodImage: {
            value: {
              src:
                'https://scdev25.duke-energy.com:443/_/media/images/svg/buildings/iconoffice.svg?h=100&la=en&w=100',
              alt: 'icon office',
              width: '100',
              height: '100',
            },
          },
          PodText: {
            value: "BUSINESSES WE'RE IN",
          },
          PodCtaText: {
            value: '',
          },
          Headline: {
            value: "BUSINESSES WE'RE IN",
          },
          ModalList: null,
          ShouldDisplayableComponent: true,
          Id: '142a6c16-8c78-45d8-b3b3-925ce007e792',
          TemplateId: '9a32b57e-563e-45dd-b8aa-aaee54de3249',
          Version: 1,
          Url: '/our-company/about-us/content/big-icon-pod-about-us/the-businesses-were-in',
          Path:
            '/sitecore/content/Public/Home/Our Company/About Us/Content/Big Icon Pod About Us/The Businesses Were In',
          Name: 'The Businesses Were In',
          RelativeUrl: '/our-company/about-us',
        },
        {
          PodItemLink: {
            value: {
              href: '/our-company/about-us/leadership',
              text: '',
              linktype: 'internal',
              class: '',
              title: '',
              querystring: '',
              id: '{05FFDECA-F001-4E5E-8B6F-9C3561D204D2}',
            },
          },
          MobilePodItemLink: {
            value: {
              href: '',
            },
          },
          PodImage: {
            value: {
              src:
                'https://scdev25.duke-energy.com:443/_/media/images/svg/people/iconspeaker.svg?h=100&la=en&w=100',
              alt: 'Image',
              width: '100',
              height: '100',
            },
          },
          PodText: {
            value: 'LEADERSHIP',
          },
          PodCtaText: {
            value: '',
          },
          Headline: {
            value: 'LEADERSHIP',
          },
          ModalList: null,
          ShouldDisplayableComponent: true,
          Id: '90b41710-b6d7-4414-a6c8-b89b194e0473',
          TemplateId: '9a32b57e-563e-45dd-b8aa-aaee54de3249',
          Version: 2,
          Url: '/our-company/about-us/content/big-icon-pod-about-us/leadership',
          Path:
            '/sitecore/content/Public/Home/Our Company/About Us/Content/Big Icon Pod About Us/Leadership',
          Name: 'Leadership',
          RelativeUrl: '/our-company/about-us',
        },
        {
          PodItemLink: {
            value: {
              href: '/our-company/about-us/ethics',
              text: '',
              linktype: 'internal',
              class: '',
              title: '',
              querystring: '',
              id: '{D1C2B499-C96D-4087-A243-71E5952B1914}',
            },
          },
          MobilePodItemLink: {
            value: {
              href: '',
            },
          },
          PodImage: {
            value: {
              src:
                'https://scdev25.duke-energy.com:443/_/media/images/svg/money/iconscales.svg?h=100&la=en&w=100',
              alt: 'Image',
              width: '100',
              height: '100',
            },
          },
          PodText: {
            value: 'ETHICS',
          },
          PodCtaText: {
            value: '',
          },
          Headline: {
            value: 'ETHICS',
          },
          ModalList: null,
          ShouldDisplayableComponent: true,
          Id: '87980fc9-b0e5-4cd6-a1cf-09445c54f811',
          TemplateId: '9a32b57e-563e-45dd-b8aa-aaee54de3249',
          Version: 1,
          Url: '/our-company/about-us/content/big-icon-pod-about-us/ethics',
          Path:
            '/sitecore/content/Public/Home/Our Company/About Us/Content/Big Icon Pod About Us/Ethics',
          Name: 'Ethics',
          RelativeUrl: '/our-company/about-us',
        },
        {
          PodItemLink: {
            value: {
              href: '/our-company/about-us/power-plants',
              text: '',
              linktype: 'internal',
              class: '',
              title: '',
              querystring: '',
              id: '{6F84AD51-9518-4884-86C9-417B87F2D85D}',
            },
          },
          MobilePodItemLink: {
            value: {
              href: '',
            },
          },
          PodImage: {
            value: {
              src:
                'https://scdev25.duke-energy.com:443/_/media/images/svg/buildings/iconpowerplant.svg?h=100&la=en&w=100',
              alt: 'icon power plant',
              width: '100',
              height: '100',
            },
          },
          PodText: {
            value: 'POWER PLANTS',
          },
          PodCtaText: {
            value: '',
          },
          Headline: {
            value: 'POWER PLANTS',
          },
          ModalList: null,
          ShouldDisplayableComponent: true,
          Id: '115273b2-b054-46cb-85bd-668073524451',
          TemplateId: '9a32b57e-563e-45dd-b8aa-aaee54de3249',
          Version: 1,
          Url: '/our-company/about-us/content/big-icon-pod-about-us/power-plants',
          Path:
            '/sitecore/content/Public/Home/Our Company/About Us/Content/Big Icon Pod About Us/Power Plants',
          Name: 'Power Plants',
          RelativeUrl: '/our-company/about-us',
        },
        {
          PodItemLink: {
            value: {
              href: '/our-company/about-us/electric-transmission-projects',
              text: '',
              linktype: 'internal',
              class: '',
              title: '',
              querystring: '',
              id: '{DFC5F4CC-02DA-4011-9730-CFC268594449}',
            },
          },
          MobilePodItemLink: {
            value: {
              href: '',
            },
          },
          PodImage: {
            value: {
              src:
                'https://scdev25.duke-energy.com:443/_/media/images/svg/distribution/iconhframetower.svg?h=100&la=en&w=100',
              alt: 'Image',
              width: '100',
              height: '100',
            },
          },
          PodText: {
            value: 'ELECTRIC TRANSMISSION PROJECTS',
          },
          PodCtaText: {
            value: '',
          },
          Headline: {
            value: 'ELECTRIC TRANSMISSION PROJECTS',
          },
          ModalList: null,
          ShouldDisplayableComponent: true,
          Id: '41ae6c73-ed6a-440c-8bf7-6e58c7369556',
          TemplateId: '9a32b57e-563e-45dd-b8aa-aaee54de3249',
          Version: 2,
          Url: '/our-company/about-us/content/big-icon-pod-about-us/electric-transmission-projects',
          Path:
            '/sitecore/content/Public/Home/Our Company/About Us/Content/Big Icon Pod About Us/Electric Transmission Projects',
          Name: 'Electric Transmission Projects',
          RelativeUrl: '/our-company/about-us',
        },
        {
          PodItemLink: {
            value: {
              href: '/our-company/about-us/smart-grid',
              text: '',
              linktype: 'internal',
              class: '',
              title: '',
              querystring: '',
              id: '{A32EC979-969B-461D-B35F-50AF22C15DED}',
            },
          },
          MobilePodItemLink: {
            value: {
              href: '',
            },
          },
          PodImage: {
            value: {
              src:
                'https://scdev25.duke-energy.com:443/_/media/images/svg/electric/iconcapacity.svg?h=100&la=en&w=100',
              alt: 'Image',
              width: '100',
              height: '100',
            },
          },
          PodText: {
            value: 'SMART GRID',
          },
          PodCtaText: {
            value: '',
          },
          Headline: {
            value: 'SMART GRID',
          },
          ModalList: null,
          ShouldDisplayableComponent: true,
          Id: '35b94094-10e6-4dfa-889c-b80926c929d9',
          TemplateId: '9a32b57e-563e-45dd-b8aa-aaee54de3249',
          Version: 2,
          Url: '/our-company/about-us/content/big-icon-pod-about-us/smart-grid',
          Path:
            '/sitecore/content/Public/Home/Our Company/About Us/Content/Big Icon Pod About Us/Smart Grid',
          Name: 'Smart Grid',
          RelativeUrl: '/our-company/about-us',
        },
        {
          PodItemLink: {
            value: {
              href: '/our-company/about-us/new-generation',
              text: '',
              linktype: 'internal',
              class: '',
              title: '',
              querystring: '',
              id: '{775F5480-DEF6-4FF7-A68C-6179050B853D}',
            },
          },
          MobilePodItemLink: {
            value: {
              href: '',
            },
          },
          PodImage: {
            value: {
              src:
                'https://scdev25.duke-energy.com:443/_/media/images/svg/technology/iconrefresh.svg?h=100&la=en&w=100',
              alt: 'Image',
              width: '100',
              height: '100',
            },
          },
          PodText: {
            value: 'NEW GENERATION',
          },
          PodCtaText: {
            value: '',
          },
          Headline: {
            value: 'NEW GENERATION',
          },
          ModalList: null,
          ShouldDisplayableComponent: true,
          Id: '204e29dc-34db-4dc0-8833-d0e27315d438',
          TemplateId: '9a32b57e-563e-45dd-b8aa-aaee54de3249',
          Version: 2,
          Url: '/our-company/about-us/content/big-icon-pod-about-us/new-generation',
          Path:
            '/sitecore/content/Public/Home/Our Company/About Us/Content/Big Icon Pod About Us/New Generation',
          Name: 'New Generation',
          RelativeUrl: '/our-company/about-us',
        },
      ],
      ShouldDisplayableComponent: true,
      Id: 'ec51d447-f4fc-4033-a5b6-410d095113ab',
      TemplateId: '66568e6d-ddc4-4b90-8237-33f21c3fd7b1',
      Version: 2,
      Url: '/our-company/about-us/content/big-icon-pod-about-us',
      Path: '/sitecore/content/Public/Home/Our Company/About Us/Content/Big Icon Pod About Us',
      Name: 'Big Icon Pod About Us',
      RelativeUrl: '/our-company/about-us',
      AnalyticsHeadline: 'More about us',
      RenderSelector: 'Big Icon',
      BackgroundColor: 'white-bg',
      OrderingType: 1,
      PodType: 2,
      IsEmptyHeadline: false,
    },
  },
};
