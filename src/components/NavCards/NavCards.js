import React from 'react';
import NavCardsComponent from './NavCardsComponent';
import getNavCardItems from './composition';
import { Data } from './NavCards.data';

const NavCards = () => {
  const getItems = getNavCardItems(Data.rectangular.fields, []);
  return <NavCardsComponent items={getItems} isOrdered={false} variant='rectangular' />;
};

export default NavCards;
