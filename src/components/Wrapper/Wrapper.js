import React, { Fragment } from 'react';
import NavCards from '../NavCards/NavCards';
import MoreInfoWidget from '../MoreInfoWidget/MoreInfoWidget';

const Wrapper = () => {
  return (
    <Fragment>
      <h2 className='text-center pt-3xl' style={{ fontSize: '30px' }}>
        NavCards
      </h2>
      <NavCards />
      <h2 className='text-center pt-3xl' style={{ fontSize: '30px' }}>
        More Info Widget
      </h2>
      <MoreInfoWidget />
    </Fragment>
  );
};

export default Wrapper;
