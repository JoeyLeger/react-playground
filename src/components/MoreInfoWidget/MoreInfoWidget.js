import React from 'react';
import MoreInfoWidgetComponent from './MoreInfoWidgetComponent';
import getMoreInfoWidgetItems from './composition';
import { Data } from './MoreInfoWidget.data';

const MoreInfoWidget = () => {
  const getItems = getMoreInfoWidgetItems(Data);
  const { body, image, text } = getItems;

  return <MoreInfoWidgetComponent body={body} image={image} text={text} />;
};

export default MoreInfoWidget;
