import React, { Fragment, useState } from 'react';
import './MoreInfoWidget.scss';

const MoreInfoWidgetComponent = ({ body, image, text }) => {
  const [expanded, setExpanded] = useState(false);

  return (
    <section className='de-more-info flex flex-col max-w-m l:max-w-l xl:max-w-xl mx-auto py-3xl text-center'>
      {image.src ? (
        <img src={image.src} alt='alt' className='de-more-info__icon mx-auto mb-s' />
      ) : null}
      <p dangerouslySetInnerHTML={{ __html: text }} />
      {body ? (
        <Fragment>
          <p
            aria-hidden={!expanded}
            className='de-more-info__body overflow-hidden'
            dangerouslySetInnerHTML={{ __html: body }}
          />
          <button
            className={`de-more-info__button relative mx-auto my-l text-s-desktop text-teal-dark-x uppercase ${
              expanded ? 'de-more-info__button--open' : ''
            }`}
            aria-expanded={expanded}
            onClick={() => setExpanded(!expanded)}
          >
            {expanded ? 'Show Less' : 'Show More'}
          </button>
        </Fragment>
      ) : null}
    </section>
  );
};

export default MoreInfoWidgetComponent;
