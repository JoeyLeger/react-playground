export const Data = {
  Icon: {
    value: {
      src:
        'https://scdev25.duke-energy.com/_/media/images/svg/account/iconinformation.svg?h=100&la=en&w=100',
      alt: 'Image',
      width: '100',
      height: '100',
    },
  },
  Subhead: {
    value:
      'Duke Energy Florida (DEF) offers optional, market-based products and services that are separate from the regulated services provided by DEF. These products and services may be available from other competitive sources.',
  },
  Body: {
    value:
      '<div></div>\nThese nonregulated utility products or services are administered by Duke Energy Florida, LLC located at 550 South Tryon Street Charlotte, NC 28202 and provided by Service Plan of Florida, Inc. located at 175 W Jackson Blvd. Chicago, IL 60604.<br />\n<br />\nNonregulated utility products and services offered by Duke Energy are not regulated or sanctioned by the Florida Public Service Commission. Customers who purchase or subscribe to receive information about Duke Energy nonregulated products will not receive preferential or special treatment from their utility company and customers are not required to purchase or subscribe in order to receive safe, reliable electric service. THIS MESSAGE IS PAID FOR BY THE SHAREHOLDERS OF DUKE ENERGY. FL Lic # QB25213.\n<div></div>',
  },
  AnalyticsLabel: {
    value: 'More Info',
  },
  ShouldDisplay: true,
  ModalList: null,
  currentPageUrl: '/home/start-stop-move',
  Id: 'a2d83ea7-a816-46c4-a448-2c0d292ed004',
  TemplateId: '8e7e0136-824d-4de7-8bf8-3d13e130da1f',
  Version: 2,
  Url: '/home/start-stop-move/content/more-info-fl',
  Path: '/sitecore/content/Public/Home/Home/Start Stop Move/Content/More Info FL',
  Name: 'More Info FL',
  RelativeUrl: '/home/start-stop-move',
};
