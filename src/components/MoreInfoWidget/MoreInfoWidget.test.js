import React from 'react';
import { mount } from 'enzyme';
import { expect } from 'chai';
import MoreInfoWidgetComponent from './MoreInfoWidgetComponent';
import getMoreInfoWidgetItems from './composition';
import { Data } from './MoreInfoWidget.data';

const getItems = getMoreInfoWidgetItems(Data);
const { body, image, text } = getItems;

describe('More Info Widget', () => {
  const wrapper = mount(<MoreInfoWidgetComponent body={body} image={image} text={text} />);

  it('renders correctly', () => {
    mount(<MoreInfoWidgetComponent body={body} image={image} text={text} />);
  });

  it('contains 1 button', () => {
    expect(wrapper.find('.de-more-info__button')).to.have.lengthOf(1);
  });

  it('renders the correct image', () => {
    expect(wrapper.find('.de-more-info__icon').first().find('[src]').props().src).to.equal(
      image.src
    );
  });
});
