const getMoreInfoWidgetItems = (fields) => {
  return {
    body: fields?.Body?.value || null,
    image: { ...fields?.Icon?.value },
    text: fields.Subhead.value,
  };
};

export default getMoreInfoWidgetItems;
